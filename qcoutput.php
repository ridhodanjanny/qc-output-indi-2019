<?php

/**
 * Quick Count Result 
 */
class QuickCount
{

	private $file_name_pilpres = "";
	private $file_name_pilpres_wilayah = "";

	public function __construct($url_pilpres, $url_pilpres_wilayah) {
		$this->file_name_pilpres = $url_pilpres;
		$this->file_name_pilpres_wilayah = $url_pilpres_wilayah;
	}

	/**
	 * Pilpres general 
	 * @return json
	 */
	public function pilpres_txt_to_json() {
 		$file_handler = fopen($this->file_name_pilpres, "r");
		$data_source = "";
		if($file_handler != false) {
			$data_source = fgets($file_handler);
		}

		$arr_data_source = explode(";", $data_source);

		$result = [];
		foreach ($arr_data_source as $urutan => $val) {
			$urutan = $urutan + 1;
			switch ($urutan) {
				case '1':
					$result["presiden_1"] = $val;
					break;
				case '2':
					$result["presiden_2"] = $val;
					break;
				case '3':
					$result["data_masuk"] = $val;
					break;
				case '4':
					$result["tingkat_partisipasi"] = $val;
					break;	
			}
		}

		echo json_encode($result, JSON_PRETTY_PRINT);

		fclose($file_handler);		
	}

	/**
	 * Pilpres per wilayah
	 * @return json
	 */
	public function pilpres_wilayah_txt_to_json() {

 		$file_handler = fopen($this->file_name_pilpres_wilayah, "r");
		$data_source = [];
		if($file_handler != false) {			
			while(!feof($file_handler)) {
				$data_source[] = fgets($file_handler);
			}
		}

		$result = [];

		foreach ($data_source as $data_per_line) {
			$data_per_provinsi = explode(";", $data_per_line);

			$result_per_line = [];
			for ($i=0; $i < count($data_per_provinsi) ; $i++) { 
				switch ($i) {
					case '0':
						$result_per_line["provinsi"] = $data_per_provinsi[$i];
						break;
					case '1':
						$result_per_line["presiden_1"] = $data_per_provinsi[$i];
						break;						
					case '2':
						$result_per_line["presiden_2"] = $data_per_provinsi[$i];
						break;
					case '3':
						$result_per_line["data_masuk"] = $data_per_provinsi[$i];
						break;
					case '4':
						$result_per_line["tingkat_partisipasi"] = $data_per_provinsi[$i];
						break;
				}
			}

			$result[] = $result_per_line;

		}

		echo json_encode($result, JSON_PRETTY_PRINT);
		fclose($file_handler);
	}

	

}

$qc = new QuickCount("http://qcindi19.com/data/PILPRES/pilpres.txt"
	, "http://qcindi19.com/data/PILPRES/pilpres_wilayah.txt");
$qc->pilpres_wilayah_txt_to_json();
// $qc->pilpres_txt_to_json();